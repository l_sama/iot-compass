# IoT Compass

[The Particle Photon](https://docs.particle.io/photon/) is a tiny Wi-Fi IoT device for creating connected projects and products for the Internet of Things. 
However, the lack of Bluetooth connection makes it difficult for the Photon to be used in interactions with mobile devices, 
particularly in those cases that require reliable stream of data from Android build-in sensors. 
The app demonstrates possibility to transfer data gathered from Android build-in sensors to Particle Photon device over Wi-Fi using Prickle Cloud capabilities. 
This particular app works as a compass which sends information about phone position every second to the cloud. 
At the same time The Particle Photon device listens to the events published in the cloud and acts on received data.
