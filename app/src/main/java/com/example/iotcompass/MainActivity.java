package com.example.iotcompass;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.ParticleEventVisibility;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;
import io.particle.android.sdk.utils.Toaster;


public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private static final String BELT_ID = "BELT_ID";
    private static final String TAG = MainActivity.class.getSimpleName();

    TextView degreesText;
    private SensorManager sensorManager;
    private Sensor rotation;
    float[] rotationMatrix = new float[9];
    float[] orientation = new float[9];
    private ParticleDevice mDevice;
    private String n;
    private SensorEvent event;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ParticleCloudSDK.init(this);

        findViewById(R.id.connect_btn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                login();

            }
        });

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        if (sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) != null) {
            rotation = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
            sensorManager.registerListener(this, rotation, SensorManager.SENSOR_DELAY_NORMAL);
        } else {
            Toast.makeText(getApplicationContext(), "Your phone is missing sensor", Toast.LENGTH_LONG).show();
        }

        degreesText = findViewById(R.id.txt_azimuth);

        sendFunction();
    }

    public void noSensorAlert() {
    }

    public void login() {

        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {


            @Override
            public Object callApi(@NonNull ParticleCloud sparkCloud) throws ParticleCloudException, IOException {
                sparkCloud.logIn("*******@*****.***", "****************"); //hardcoded email address and password
                mDevice = sparkCloud.getDevice("********************************"); // the controller ID
                return -1;
            }

            @Override
            public void onSuccess(@NonNull Object value) {
                Toaster.l(MainActivity.this, "Logged in");
                if (mDevice.isConnected()) {
                    Toaster.l(MainActivity.this, mDevice.toString());
                }
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Toaster.s(MainActivity.this, "Something went wrong");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, rotation, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this, rotation);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sensorManager = null;
        rotation = null;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            SensorManager.getRotationMatrixFromVector(rotationMatrix, event.values);

            int azimuth = (int) ((Math.toDegrees(SensorManager.getOrientation(rotationMatrix, orientation)[0]) + 360) % 360);
            degreesText.setText("Azimuth : " + azimuth + "° " + n);


            if (azimuth >= 337 && azimuth <= 360 || azimuth >= 0 && azimuth <= 22) {
                n = "N"; // 0 deg

            } else if (azimuth > 22 && azimuth < 68) {
                n = "NE"; //45 deg

            } else if (azimuth >= 68 && azimuth <= 112) {
                n = "E"; // 90deg

            } else if (azimuth > 112 && azimuth < 157) {
                n = "SE"; //135deg

            } else if (azimuth >= 157 && azimuth <= 203) {
                n = "S"; //180deg

            } else if (azimuth > 203 && azimuth < 248) {
                n = "SW"; //225deg

            } else if (azimuth >= 248 && azimuth <= 293) {
                n = "W"; //270deg

            } else if (azimuth > 293 && azimuth < 337) {
                n = "NW";//315deg

            }
        }
    }

    public void sendFunction() {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {

                try {
                    ParticleCloudSDK.getCloud().publishEvent("Direction", n, ParticleEventVisibility.PUBLIC, 60);
                } catch (ParticleCloudException e) {
                    e.printStackTrace();
                }
            }
        };
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 1, 1000);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

}



